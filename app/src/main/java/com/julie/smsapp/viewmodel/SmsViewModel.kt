package com.julie.smsapp.viewmodel

import androidx.lifecycle.ViewModel
import com.julie.smsapp.model.JsonItem
import com.julie.smsapp.remote.SmsCall
import com.julie.smsapp.repos.SmsRepo
import retrofit2.Response

class SmsViewModel() : ViewModel() {

    val smsCall = SmsCall()
    val smsRepo = SmsRepo(smsCall = smsCall)


    suspend fun getTodoItems(): Response<JsonItem> = smsRepo.getTodoItems()
}