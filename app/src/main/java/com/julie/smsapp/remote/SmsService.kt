package com.julie.smsapp.remote

import com.julie.smsapp.model.JsonItem
import retrofit2.Response
import retrofit2.http.GET


interface SmsService {
    @GET("todos/1")
    suspend fun getTodoItems(): Response<JsonItem>

    @GET("posts")
    suspend fun posts(): Response<JsonItem>

}