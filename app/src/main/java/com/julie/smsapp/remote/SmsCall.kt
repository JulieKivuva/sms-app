package com.julie.smsapp.remote

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class SmsCall {

    fun retrofitInstance(): SmsService {

//        private fun provideLogger(): OkHttpClient {
//            val interceptor = HttpLoggingInterceptor()
//            interceptor.level = HttpLoggingInterceptor.Level.BODY
//
//
//            return OkHttpClient.Builder()
//                .addInterceptor(interceptor)
//                .addInterceptor { chain ->
//                    val newRequest = chain.request().newBuilder()
//                        .addHeader("Content-Type", "application/json")
//                        .build()
//                    chain.proceed(newRequest)
//                }
//
//
//                .connectTimeout(30, TimeUnit.SECONDS)
//                .writeTimeout(30, TimeUnit.SECONDS)
//                .readTimeout(30, TimeUnit.SECONDS)
//                .build()
//        }

        val gson = GsonBuilder().create()

        val retrofit = Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/")
//            .client(prov)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        return retrofit.create(SmsService::class.java)
    }
}
