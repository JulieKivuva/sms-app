package com.julie.smsapp.repos

import com.julie.smsapp.model.JsonItem
import com.julie.smsapp.remote.SmsCall
import com.julie.smsapp.smsCall
import retrofit2.Response


class SmsRepo (smsCall: SmsCall){

    suspend fun getTodoItems():Response<JsonItem> = smsCall().getTodoItems()

    suspend fun posts():Response<JsonItem> = smsCall().posts()



}
