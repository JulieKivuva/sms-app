package com.julie.smsapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.activity.viewModels
import com.google.gson.GsonBuilder
import com.julie.smsapp.remote.SmsService
import com.julie.smsapp.viewmodel.SmsViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    private val viewModel:SmsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val userId = findViewById<TextView>(R.id.user_id)
        val title = findViewById<TextView>(R.id.title)
        val completed = findViewById<TextView>(R.id.completed)


        GlobalScope.launch {

            val jsonItem = viewModel.getTodoItems().body()

//            val jsonItem = smsCall().getTodoItems().body()

            runOnUiThread {
                userId.text = jsonItem?.userId.toString()
                title.text = jsonItem?.title
                completed.text = jsonItem?.completed.toString()
            }

        }

    }
}

fun smsCall(): SmsService {

    val gson = GsonBuilder().create()

    val retrofit = Retrofit.Builder()
        .baseUrl("https://jsonplaceholder.typicode.com/")
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    return retrofit.create(SmsService::class.java)
}